<?php

return [

    'client' => [
        'list' => 'Lista de usuarios',
        'name' => 'Nombre',
        'phone' => 'Telefono',
        'email' => 'Email',
        'type' => 'Tipo',
        'from' => 'Origen',
        'no_clients' => 'No hay usuarios',
        'information' => 'Datos del usuario',
        'contacts' => 'Contactos',
        'status' => 'Estado',
        'actions' => 'Acciones',
        'nif' => 'NIF',
        'billing_address' => 'Dirección de facturación',
        'street' => 'Calle',
        'floor' => 'Piso',
        'postal_code' => 'Código postal',
        'town' => 'Población',
        'province' => 'Provincía',
        'country' => 'País',
        'no_shipping' => 'No hay direcciones de envío',
        'main_shipping_address' => 'Dirección principal',
        'last_session' => 'Última conexión',
        'sessions' => 'Conexiones',
    ],

    'session' => [
        'list' => 'Lista de conexiones',
        'ip' => 'IP',
        'day' => 'Fecha',
        'no_sessions' => 'No hay conexiones',
    ],

    'create' => [
        'title' => 'Añadir Usuario',
    ]
];
