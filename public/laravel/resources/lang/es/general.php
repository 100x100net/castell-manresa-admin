<?php

return [

    'menu' => [
        'menu' => 'Menú',
        'dashboard' => 'Dashboard',
        'clients' => 'Usuarios',
        'list' => 'Lista',
        'add_new' => 'Añadir nuevo',
        'homepage' => 'Página de inicio',
        'create' => 'Nuevo usuario',
    ],

    'msg' => [
        'done' => '¡Hecho!',
        'ups' => '¡Uy!',
        'check_fields' => 'Comprueba todos los campos',
        'shipping_updated' => 'La dirección de envío ha sido actualizada',
        'client_updated' => 'Los datos del cliente han sido actualizados',
        'contact_updated'  => 'Los datos del contacto han sido actualizados',
        'product_updated'  => 'Los datos del producto han sido actualizados',
        'try_later' => 'Inténtelo más tarde, por favor',
        'offer_added' => 'La oferta se ha creado correctamente',
        'offer_deleted' => 'La oferta se ha eliminado correctamente',
        'offer_updated' => 'La oferta se ha actualizado correctamente',
        'promo_added' => 'La promoción se ha creado correctamente',
        'promo_deleted' => 'La promoción se ha eliminado correctamente',
        'promo_updated' => 'La promoción se ha actualizado correctamente',
        'seo_updated' => 'El SEO se ha actualizado correctamente',
        'images_uploaded' => 'Las imágenes se han guardado correctamente',
        'image_deleted' => 'La imagen se han eliminado correctamente',
        'principal_offer_updated' => 'La oferta principal se ha actualizado correctamente',
        'featured_products_updated' => 'Los productos destacados se han actualizado correctamente'

    ],

    'save' => 'Guardar',
    'accept' => 'Aceptar',
    'view' => 'Ver',
    'edit' => 'Editar',

    'forms' => [
        'labels' => [
            'name' => 'Nombre',
            'email' => 'Email',
            'company' => 'Empresa',
            'phone' => 'Telefono',
            'password' => 'Contraseña (12 caracteres minimo)',
        ],
        'send' => 'Guardar',
    ],

];
