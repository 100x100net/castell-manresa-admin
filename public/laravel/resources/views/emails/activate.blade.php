<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Facebook sharing information tags -->
    <meta property="og:title" content="*|MC:SUBJECT|*">
    <title>CASTELL DE MANRESA REGISTER REQUEST</title>
     <style type="text/css">
        #outlook a {
            padding: 0;
        }

        body {
            width: 100% !important;
            -webkit-text-size-adjust: none;
            margin: 0;
            padding: 0;
            font-family: Georgia, serif;
            color: #c56e51;
            line-height:150%;
            font-size:18px;
        }

        img {
            border: none;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            text-transform: capitalize;
        }

        @media only screen and (max-width: 480px) {
            .h1 {
                font-size: 40px !important;
                line-height: 100% !important;
            }
        }

       
    </style>
</head>

<body style="margin:0; padding:0; background-color: #FFFEF8;" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
    <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%" style="border-collapse:collapse;">
        <tbody>
        <tr>
            <td valign="top" align="center">
                <table cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;padding:30px;margin-top:40px;background-color: #FFFEF8;">
                    <tbody>
                    <tr>
                        <td valign="top" align="center">
                            <p style="text-align:center;">
                                <img src="{{asset('assets/img/logo-castellmanresa.png')}}" alt=""></p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding:20px 50px;margin:10px 0;font-size:18px;font-family: Georgia, serif;color:#c56e51;">
                            <h2>Your account has been activated!</h2>
                            <p style="text-align: center;">Your access data is:</p>
                            <ul style="text-align: center; list-style: none; padding: 0px;">
                                <li>Email: {{$user->email}}</li>
                                <li>Password: {{$user->password_literal}}</li>
                            </ul>
                        </td>
                    </tr>
                   
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</center>
</body>

</html>
