@extends('layouts.app')

@section('styles')
<style>
    html,
    main, body {
        height: 100%;
    }

    main {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    .invalid-feedback{
        display: block;
    }
    .card-footer-item-full{
        width: 100%;
    }
</style>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- forgot password  -->
<!-- ============================================================== -->
<div class="splash-container">
    <div class="card">
        <div class="card-header text-center"><img class="logo-img" src="{{ asset('assets/images/logo.png') }}" alt="logo"><span class="splash-description">{{ __('Reset Password') }}</span></div>
        <div class="card-body">
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <p>{{ __("Please reset the password for your account.") }}</p>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required="" placeholder="{{ __('E-Mail Address') }}" autocomplete="off">

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <input class="form-control form-control-lg" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required="" placeholder="{{ __('Password') }}" autocomplete="off">

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <input class="form-control form-control-lg" id="password_confirmation" type="password" class="form-control" name="password_confirmation" required="" placeholder="{{ __('Password') }}">
                </div>

                <div class="form-group pt-1">
                    <button type="submit" class="btn btn-block btn-primary btn-xl">
                        {{ __('Reset Password') }}
                    </button>
                </div>  
            </form>
        </div>
        <div class="card-footer text-center">
            <span><a href="{{ route('login') }}">{{ __('Return to login') }} </a></span>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end forgot password  -->
<!-- ============================================================== -->
@endsection

@section('scripts')

@endsection