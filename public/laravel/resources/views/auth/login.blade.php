@extends('layouts.app')

@section('styles')
<style>
    html,
    main, body {
        height: 100%;
    }

    main {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    .invalid-feedback{
        display: block;
    }
    .card-footer-item-full{
        width: 100%;
    }
</style>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- login page  -->
<!-- ============================================================== -->
<div class="splash-container">
    <div class="card ">
        <div class="card-header text-center" style="background-color: #002638 !important;"><a href="#"><img style="width: 100%;" class="logo-img" src="{{ asset('assets/img/logo-title.svg') }}" alt="logo"></a></div>
        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <input class="form-control form-control-lg"  id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('E-Mail Address') }}" required autofocus autocomplete="off">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <input class="form-control form-control-lg" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" required="">

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="custom-control-label">{{ __('Remember Me') }}</span>
                    </label>
                </div>
                <button type="submit" class="btn btn-outline-light btn-lg btn-block">{{ __('Sign in') }}</button>
            </form>
        </div>
        <div class="card-footer bg-white p-0">
            <div class="card-footer-item card-footer-item-bordered card-footer-item-full">
                <a href="{{ route('password.request') }}" class="footer-link">{{ __('Forgot Password') }}</a>
            </div>
        </div>
    </div>
</div>

<!-- ============================================================== -->
<!-- end login page  -->
<!-- ============================================================== -->
@endsection

@section('scripts')

@endsection
