<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="{{ asset('assets/vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/fontawesome/css/fontawesome-all.css') }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/libs/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/libs/css/datatables.scss') }}">
    <link rel="stylesheet" href="{{ asset('assets/libs/css/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/libs/css/basic.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/vendor/charts/chartist-bundle/chartist.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/charts/morris-bundle/morris.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/charts/c3charts/c3.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/flag-icon-css/flag-icon.min.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

    @yield('styles')

</head>
<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        @guest
            <main>
                @yield('content')
            </main>
        @else
            @include('layouts.header')
            <main class="dashboard-wrapper">
                @yield('content')

                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                <div class="footer">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                         Copyright © Urban Input. All rights reserved.
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end footer -->
                <!-- ============================================================== -->

            </main>
        @endguest
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->

    <div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="deleteUserModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Eliminar usuario
                </div>
                <div class="modal-body">
                    Estas seguro de eleminiar este usuario?<br>Se borraran todos sus datos relacionados.
                </div>
                <div class="modal-footer">
                    <button id="cancel-delete-button" type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-sm btn-danger" href="{{ route('user.delete') }}" onclick="event.preventDefault(); document.getElementById('user-delete').submit();">Eliminar</a>
                    <form id="user-delete" action="{{ route('user.delete') }}" method="POST" style="display: none;">
                        @csrf
                        <input type="hidden" value="" name="user_id">
                    </form>
                </div>
            </div>
        </div>
    </div>

<!-- Scripts -->
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/vendor/parsley/parsley.js') }}"></script>
<script src="{{ asset('assets/libs/js/main-js.js') }}"></script>

<!-- Optional JavaScript -->

<!-- sparkline js -->
<script src="{{ asset('assets/vendor/charts/sparkline/jquery.sparkline.js') }}"></script>
<!-- morris js -->
<script src="{{ asset('assets/vendor/charts/morris-bundle/raphael.min.js') }}"></script>
<script src="{{ asset('assets/vendor/charts/morris-bundle/morris.js') }}"></script>

<script src="{{ asset('assets/libs/js/datatables.js') }}"></script>
<script src="{{ asset('assets/libs/js/dropzone.js') }}"></script>

@yield('scripts')

</body>
</html>
