@extends('layouts.app')

@section('content')
<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <p class="pageheader-text"></p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active" aria-current="page">{{ __('general.menu.clients') }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader  -->
        <!-- ============================================================== -->
        <div class="ecommerce-widget">

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ $message }}</strong>
                        </div>
                    @endif

                    @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ $message }}</strong>
                        </div>
                    @endif

                    <div class="card">
                        <h5 class="card-header">{{ __('pages.client.list') }}</h5>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table" data-page-length='10' class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">{{ __('pages.client.name') }}</th>
                                        <th scope="col">{{ __('pages.client.email') }}</th>
                                        <th scope="col">{{ __('pages.client.phone') }}</th>
                                        <th scope="col">{{ __('pages.client.sessions') }}</th>
                                        <th scope="col">{{ __('pages.client.last_session') }}</th>
                                        <th scope="col">{{ __('pages.client.status') }}</th>
                                        <th scope="col">{{ __('pages.client.actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (sizeof($users) > 0)
                                        @foreach ($users as $user)
                                            <tr>
                                                <td scope="row">{{$user->id}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->phone}}</td>
                                                <td>{{$user->session_number}}</td>
                                                <td>{{$user->last_session}}</td>
                                                @if($user->active)
                                                    <td>ACTIVO</td>
                                                    <td>
                                                        <a class="btn btn-sm btn-warning" href="{{ route('user.deactive', $user->id) }}" onclick="event.preventDefault(); document.getElementById('user-deactivate-{{$user->id}}').submit();">Desactivar</a>
                                                        <form id="user-deactivate-{{$user->id}}" action="{{ route('user.deactive', $user->id) }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                        <a class="btn btn-sm btn-primary" href="{{ route('user.show', $user->id) }}">Conexiones</a>
                                                    </td>
                                                @else
                                                    <td>NO ACTIVO</td>
                                                    <td>
                                                        <a class="btn btn-sm btn-success" href="{{ route('user.deactive', $user->id) }}" onclick="event.preventDefault(); document.getElementById('user-activate-{{$user->id}}').submit();">Activar</a>
                                                        <form id="user-activate-{{$user->id}}" action="{{ route('user.active', $user->id) }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>

                                                        <button type="button" class="btn btn-sm btn-danger delete-button" data-user="{{$user->id}}" data-toggle="modal" data-target="#deleteUserModal">Eliminar</button>

                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td align="center" height="100" colspan="5">{{ __('pages.client.no_clients') }}</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script>
    $(document).on('ready', function() {
        $('#table').DataTable({
            ordering: false,
            bInfo : false
        });

        $('*[data-href]').on('click', function() {
            window.location = $(this).data("href");
        });


        $('.delete-button').on('click', function() {
            $id = $(this).attr('data-user');
            $("#user-delete input[name='user_id']").val($id);
        });

        $('#cancel-delete-button').on('click', function() {
            $("#user-delete input[name='user_id']").val(null);
        });
    });
</script>
@endsection
