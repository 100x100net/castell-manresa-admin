@extends('layouts.app')

@section('content')
<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <p class="pageheader-text"></p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}" class="breadcrumb-link">{{ __('general.menu.clients') }}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{$user->name}}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader  -->
        <!-- ============================================================== -->
        <div class="ecommerce-widget">

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">{{ __('pages.session.list') }}</h5>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table" data-page-length='10' class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">{{ __('pages.session.ip') }}</th>
                                        <th scope="col">{{ __('pages.session.day') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (sizeof($user->sessions) > 0)
                                        @foreach ($user->sessions as $session)
                                            <tr>
                                                <td scope="row">{{$session->id}}</td>
                                                <td>{{$session->ip}}</td>
                                                <td>{{$session->created_at}}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td align="center" height="100" colspan="5">{{ __('pages.session.no_sessions') }}</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <script>
        $(document).on('ready', function() {
            $('#table').DataTable({
                ordering: false,
                bInfo : false
            });

            $('*[data-href]').on('click', function() {
                window.location = $(this).data("href");
            });
        });
    </script>
@endsection
