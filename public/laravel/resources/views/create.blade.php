@extends('layouts.app')

@section('content')
<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <p class="pageheader-text"></p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}" class="breadcrumb-link">{{ __('general.menu.clients') }}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{ __('general.menu.create') }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader  -->
        <!-- ============================================================== -->
        <div class="ecommerce-widget">

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">{{ __('pages.create.title') }}</h5>
                        <div class="card-body">
                            <form action="{{route('user.insert')}}" class="row" method="POST" id="insertUser">
                                @csrf
                                    
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    
                                    <div class="form-group {{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <label for="name">{{ __('general.forms.labels.name') }}</label>
                                        <input name="name" id="name" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') ? old('name') : null }}" required>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('company') ? ' has-danger' : '' }}">
                                        <label for="company">{{ __('general.forms.labels.company') }}</label>
                                        <input name="company" id="company" type="text" class="form-control {{ $errors->has('company') ? ' is-invalid' : '' }}" value="{{ old('company') ? old('company') : null }}" required>
                                        @if ($errors->has('company'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('company') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <label for="email">{{ __('general.forms.labels.email') }}</label>
                                        <input name="email" id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') ? old('email') : null }}" required>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                </div>


                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    
                                    <div class="form-group {{ $errors->has('phone') ? ' has-danger' : '' }}">
                                        <label for="phone">{{ __('general.forms.labels.phone') }}</label>
                                        <input name="phone" id="phone" type="text" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') ? old('phone') : null }}" required>
                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label for="password">{{ __('general.forms.labels.password') }}</label>
                                        <input name="password" minlength="12" maxlength="100" id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{ old('password') ? old('password') : null }}" required>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <input style="float: right; position: absolute; bottom: 12px; right: 15px;" class="btn btn-lg btn-outline-light" type="submit" value="{{ __('general.forms.send') }}">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
@endsection
