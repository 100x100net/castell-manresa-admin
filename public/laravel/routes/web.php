<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);
Route::get('/', 'HomeController@index')->name('home');

Route::get('/users/create', 'HomeController@userCreate')->name('user.create');
Route::post('/users/insert', 'HomeController@userInsert')->name('user.insert');
Route::post('/users/delete', 'HomeController@userDelete')->name('user.delete');

Route::get('/users/{id}', 'HomeController@userShow')->name('user.show');
Route::post('/users/{id}/deactivate', 'HomeController@userDeactivate')->name('user.deactive');
Route::post('/users/{id}/activate', 'HomeController@userActivate')->name('user.active');

