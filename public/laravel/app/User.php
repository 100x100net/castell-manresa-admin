<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'active', 'name', 'company', 'phone', 'email', 'email_verified_at'
    ];

    /* RELACIONS 1-N */
    public function sessions()
    {
        return $this->hasMany('App\UserSession', 'id_user', 'id');
    }

}
