<?php

namespace App\Http\Controllers;

use App\HomePrincipalOffer;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use App\Mail\ActiveUserEmail;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\UserSession;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::with('sessions')->get();

        foreach ($users as $key => $user) {
            $user->session_number = $user->sessions->count();
            if( $user->sessions->count() >= 1 ){
                $user->last_session = $user->sessions[$user->sessions->count() - 1]->created_at;
            }else{
                $user->last_session = null;
            }
        }
        
        return view('list', ['users' => $users]);
    }

    public function userDeactivate($id){
        $user = User::findOrFail($id);
        $user->active = 0;
        $user->save();

        return redirect()->back();
    }

    public function userActivate($id){

        $newPassword = $this->randomPassword();

        $user = User::findOrFail($id);
        $user->active = 1;
        $user->password = \Hash::make($newPassword);
        $user->save();

        $user->password_literal = $newPassword;

        // Send Emilio
        Mail::to( $user->email )->send( new ActiveUserEmail( $user ) );

        return redirect()->back();
    }

    public function userShow($id){
        $user = User::with('sessions')->findOrFail($id);
        return view('show', ['user' => $user]);
    }

    private function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = '';
        for ($i = 0; $i < 12; $i++) {
            $n = rand( 0, strlen($alphabet) - 1 );
            $pass .= $alphabet[$n];
        }
        return $pass;
    }

    public function userCreate(){
        return view('create');
    }

    public function userInsert(Request $request){

        $data = $request->all();

        $validatorResults = Validator::make($data, [
            'name' => ['required', 'string', 'max:190'],
            'phone' => ['required', 'string', 'max:190'],
            'email' => ['required', 'string', 'email', 'max:190', 'unique:users,email'],
            'company' => ['required', 'string', 'max:190'],
            'password' => ['required', 'string', 'min:12'],
        ]);
        

        if( $validatorResults->passes() ){
            $user = new User($request->all());
            $user->password = \Hash::make($request->get('password'));
            $user->active = 1;
            $user->save();

            $user->password_literal = $request->get('password');

            // Send Emilio
            Mail::to( $user->email )->send( new ActiveUserEmail( $user ) );

            return redirect()->route('home')->withSuccess('Enhorabuena! El usuario ha sido creado con exito!');

        }else{
            $aux = $validatorResults->errors();
            return redirect()->back()->withInput($request->all())->withErrors($aux);
        }
    }


    public function userDelete(Request $request){

        $id = $request->get('user_id');

        $user = User::findOrFail($id);
        $user->delete();

        $userSessions = UserSession::where('id_user', $id)->get();
        foreach ($userSessions as $key => $ses) {
            $ses->delete();
        }

        return redirect()->route('home')->withError('El usuario ha sido borrado con exito!');

    }

}
